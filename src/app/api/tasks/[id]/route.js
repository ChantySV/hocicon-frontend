import { NextResponse } from "next/server";

export async function PATCH(req, { params }) {
  try {
    
    const { id } = params;
    if (!id) {
      return NextResponse.error('ID no proporcionado en la solicitud', 400);
    }
    const url = `http://localhost:5000/noticias/${id}`;
    
    const requestBody = await req.text();
    const requestData = JSON.parse(requestBody);

    const response = await fetch(url, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    });

    if (response.ok) {
      console.log('Actualización exitosa');
      return NextResponse.json({ message: 'Actualización exitosa' });
    } else {
      console.error('Error al actualizar datos', response.statusText);
      return NextResponse.error('Error al actualizar datos', response.status);
    }
  } catch (error) {
    console.error('Error en la solicitud PATCH', error);
    return NextResponse.error('Error en la solicitud PATCH', 500);
  }
}

export async function DELETE(req, { params }) {
  try {    
    const { id } = params;
    if (!id) {
      return NextResponse.error('ID no proporcionado en la solicitud', 400);
    }    
    const url = `http://localhost:5000/noticias/${id}`;
        
    const response = await fetch(url, {
      method: 'DELETE',
    });

    if (response.ok) {
      console.log('Eliminación exitosa');
      return NextResponse.json({ message: 'Eliminación exitosa' }); 
    } else {
      console.error('Error al eliminar datos', response.statusText);
      return NextResponse.error('Error al eliminar datos', response.status);
    }
  } catch (error) {
    console.error('Error en la solicitud DELETE', error);
    return NextResponse.error('Error en la solicitud DELETE', 500);
  }
}
