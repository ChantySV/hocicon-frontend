import { NextResponse } from "next/server";

export async function GET() {
  try {
    const res = await fetch("http://localhost:5000/noticias");
    const data = await res.json();

    return NextResponse.json(data);
  } catch (error) {
    console.error("Error al realizar la solicitud GET:", error);
    return NextResponse.error("Error interno del servidor", 500);
  }
}

export async function POST(req) {
  try {
    const requestBody = await req.text();
    const requestData = JSON.parse(requestBody);

    const res = await fetch("http://localhost:5000/noticias", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(requestData),
    });

    if (res.ok) {
      const responseData = await res.json();
      return NextResponse.json(responseData);
    } else {
      console.error("Error en la solicitud POST:", res.status);
      return NextResponse.error("Error en la solicitud POST", res.status);
    }
  } catch (error) {
    console.error("Error al procesar la solicitud POST:", error);
    return NextResponse.error("Error interno del servidor", 500);
  }
}
