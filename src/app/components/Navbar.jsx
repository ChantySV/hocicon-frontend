// components/Navbar.jsx
import React from 'react';
import { AppBar, Toolbar, Typography, Button, Link } from '@mui/material';
import NextLink from 'next/link';


const Navbar = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <NextLink href="/" passHref>
          <Link variant="h6" color="inherit" underline="none" style={{ flexGrow: 1 }}>
            El Hocicón
          </Link>
        </NextLink>
        <NextLink href="/new" passHref>
          <Link color="inherit">Home</Link>
        </NextLink>
        <NextLink href="/new" passHref>
          <Link color="inherit">Agregar Noticias</Link>
        </NextLink>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
