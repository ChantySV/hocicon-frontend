'use client'
import { useState } from 'react';

const NoticiaForm = () => {
  const [formulario, setFormulario] = useState({
    titulo: '',
    lugarPublicacion: '',
    autor: '',
    contenido: '',
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormulario((prevFormulario) => ({
      ...prevFormulario,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const res = await fetch('http://localhost:5000/noticias', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formulario),
      });

      if (res.ok) {
        const responseData = await res.json();
        console.log('Respuesta POST:', responseData);
      } else {
        console.error('Error en la solicitud POST:', res.status);
      }
    } catch (error) {
      console.error('Error al procesar la solicitud POST:', error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Título:
        <input
          type="text"
          name="titulo"
          value={formulario.titulo}
          onChange={handleChange}
        />
      </label>
      <br />
      <label>
        Lugar de Publicación:
        <input
          type="text"
          name="lugarPublicacion"
          value={formulario.lugarPublicacion}
          onChange={handleChange}
        />
      </label>
      <br />
      <label>
        Autor:
        <input
          type="text"
          name="autor"
          value={formulario.autor}
          onChange={handleChange}
        />
      </label>
      <br />
      <label>
        Contenido:
        <textarea
          name="contenido"
          value={formulario.contenido}
          onChange={handleChange}
        />
      </label>
      <br />
      <button type="submit">Enviar</button>
    </form>
  );
};

export default NoticiaForm;
