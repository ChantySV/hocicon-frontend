"use client";
import React, { useEffect, useState } from "react";
import { Grid, Paper, Typography, Button, Dialog, DialogTitle, DialogContent, TextField, DialogActions } from "@mui/material";

const NoticiasGrid = () => {
  const [noticias, setNoticias] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [selectedNoticia, setSelectedNoticia] = useState(null);

  const fetchNoticias = async () => {
    try {
      const response = await fetch("http://localhost:5000/noticias");
      const data = await response.json();
      setNoticias(data);
    } catch (error) {
      console.error("Error fetching noticias:", error);
    }
  };

  useEffect(() => {
    // Cargar noticias al montar el componente
    fetchNoticias();
  }, []);

  const handleDelete = async (id) => {
    try {
      const res = await fetch(`/api/tasks/${id}`, {
        method: "DELETE",
      });

      if (res.ok) {
        // Actualizar el estado de las noticias después de eliminar con éxito
        setNoticias((prevNoticias) =>
          prevNoticias.filter((noticia) => noticia.id !== id)
        );

        // Llamar a fetchNoticias para actualizar la lista después de la eliminación
        fetchNoticias();
      } else {
        console.error("Error al eliminar la noticia:", res.status);
      }
    } catch (error) {
      console.error("Error en la solicitud DELETE:", error);
    }
  };

  const handleUpdate = (noticia) => {
    setSelectedNoticia(noticia);
    setOpenDialog(true);
  };

  const handleDialogClose = () => {
    setOpenDialog(false);
    setSelectedNoticia(null);
  };

  const handleUpdateSubmit = async () => {
    // Aquí deberías implementar la lógica para enviar la solicitud de actualización
    // Puedes usar fetch o una librería como axios
    // Después de la actualización, cierra el diálogo y actualiza la lista de noticias
    setOpenDialog(false);
    setSelectedNoticia(null);
    fetchNoticias();
  };

  return (
    <Grid container spacing={2}>
      {noticias.map((noticia) => (
        <Grid item key={noticia.id} xs={12} md={6} lg={4}>
          <Paper style={{ padding: "16px" }}>
            <Typography variant="h6">{noticia.titulo}</Typography>
            <Typography variant="subtitle1">{`Fecha de Publicación: ${noticia.fechaPublicacion}`}</Typography>
            <Typography variant="subtitle1">{`Lugar de Publicación: ${noticia.lugarPublicacion}`}</Typography>
            <Typography variant="subtitle1">{`Autor: ${noticia.autor}`}</Typography>
            <Typography variant="body2">{noticia.contenido}</Typography>
            <Button
              variant="outlined"
              color="error"
              onClick={() => handleDelete(noticia.uuid)}
            >
              Eliminar
            </Button>
            <Button
              variant="outlined"
              color="primary"
              onClick={() => handleUpdate(noticia.uuid)}
            >
              Actualizar
            </Button>
          </Paper>
        </Grid>
      ))}
      <Dialog open={openDialog} onClose={handleDialogClose}>
        <DialogTitle>Actualizar Noticia</DialogTitle>
        <DialogContent>
          <TextField
            label="Título"
            fullWidth
            defaultValue={selectedNoticia?.titulo}
          />
          {/* Agrega otros campos del formulario según tu modelo de datos */}
          <TextField
            label="Título"
            fullWidth
            defaultValue={selectedNoticia?.titulo}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialogClose} color="secondary">
            Cancelar
          </Button>
          <Button onClick={handleUpdateSubmit} color="primary">
            Actualizar
          </Button>
        </DialogActions>
      </Dialog>
    </Grid>
  );
};

export default NoticiasGrid;
